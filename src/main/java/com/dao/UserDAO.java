package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.User;

@Service
public class UserDAO {
	
	@Autowired
	private UserRepo userRepo;
	
	public List<User> getAllCustomers() {
		// TODO Auto-generated method stub
		return userRepo.findAll();
	}

	public void register(User customer) {
		// TODO Auto-generated method stub
		userRepo.save(customer);
	}

	public User getCustomerById(int id) {
		// TODO Auto-generated method stub
		return userRepo.findById(id).orElse(new User());
	}

	public User getValidateCustomer(String email, String password) {
		// TODO Auto-generated method stub
		return userRepo.validateCustLogin(email,password);
	}

	public User deleteCustomerById(int id) {
		 User user = getCustomerById(id);
	        if (user != null) {
	            userRepo.deleteById(id);
	        }
	        return user;
	    }


	public User updateCustomer(User customer) {
	    User existingCustomer = userRepo.findById(customer.getId()).orElse(null);
	    
	    if (existingCustomer != null) {
	        existingCustomer.setEmail(customer.getEmail());
	        existingCustomer.setPassword(customer.getPassword());
	        return userRepo.save(existingCustomer);
	    } else {
	        return null;
	    }
	}

	
}
