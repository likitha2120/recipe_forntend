import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegKababComponent } from './veg-kabab.component';

describe('VegKababComponent', () => {
  let component: VegKababComponent;
  let fixture: ComponentFixture<VegKababComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VegKababComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VegKababComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
