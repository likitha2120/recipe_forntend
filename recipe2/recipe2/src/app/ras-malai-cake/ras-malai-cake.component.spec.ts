import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RasMalaiCakeComponent } from './ras-malai-cake.component';

describe('RasMalaiCakeComponent', () => {
  let component: RasMalaiCakeComponent;
  let fixture: ComponentFixture<RasMalaiCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RasMalaiCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RasMalaiCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
