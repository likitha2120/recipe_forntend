import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftoverRotiFrysComponent } from './leftover-roti-frys.component';

describe('LeftoverRotiFrysComponent', () => {
  let component: LeftoverRotiFrysComponent;
  let fixture: ComponentFixture<LeftoverRotiFrysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LeftoverRotiFrysComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LeftoverRotiFrysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
