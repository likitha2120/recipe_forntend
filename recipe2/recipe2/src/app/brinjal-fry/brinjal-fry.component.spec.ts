import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrinjalFryComponent } from './brinjal-fry.component';

describe('BrinjalFryComponent', () => {
  let component: BrinjalFryComponent;
  let fixture: ComponentFixture<BrinjalFryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BrinjalFryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BrinjalFryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
