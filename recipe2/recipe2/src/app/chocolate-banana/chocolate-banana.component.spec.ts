import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChocolateBananaComponent } from './chocolate-banana.component';

describe('ChocolateBananaComponent', () => {
  let component: ChocolateBananaComponent;
  let fixture: ComponentFixture<ChocolateBananaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChocolateBananaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ChocolateBananaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
