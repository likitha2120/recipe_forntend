import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuttonHaleemComponent } from './mutton-haleem.component';

describe('MuttonHaleemComponent', () => {
  let component: MuttonHaleemComponent;
  let fixture: ComponentFixture<MuttonHaleemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MuttonHaleemComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MuttonHaleemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
