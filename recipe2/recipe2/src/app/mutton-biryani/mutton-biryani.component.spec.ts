import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MuttonBiryaniComponent } from './mutton-biryani.component';

describe('MuttonBiryaniComponent', () => {
  let component: MuttonBiryaniComponent;
  let fixture: ComponentFixture<MuttonBiryaniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MuttonBiryaniComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MuttonBiryaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
