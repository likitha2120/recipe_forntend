import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MangoMilkShakesComponent } from './mango-milk-shakes.component';

describe('MangoMilkShakesComponent', () => {
  let component: MangoMilkShakesComponent;
  let fixture: ComponentFixture<MangoMilkShakesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MangoMilkShakesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MangoMilkShakesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
