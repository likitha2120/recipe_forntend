import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrl: './recipe.component.css'
})
export class RecipeComponent implements OnInit {

  items:any;
  constructor(private service:CustomerService){
    
  }

  ngOnInit(): void {
    this.service.getAllItems().subscribe((data:any)=>{
      this.items=data;
      console.log(data);
    })
    
  }
}
