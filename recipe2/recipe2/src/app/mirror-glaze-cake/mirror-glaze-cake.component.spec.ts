import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MirrorGlazeCakeComponent } from './mirror-glaze-cake.component';

describe('MirrorGlazeCakeComponent', () => {
  let component: MirrorGlazeCakeComponent;
  let fixture: ComponentFixture<MirrorGlazeCakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MirrorGlazeCakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MirrorGlazeCakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
