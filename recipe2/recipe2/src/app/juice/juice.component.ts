import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-juice',
  templateUrl: './juice.component.html',
  styleUrl: './juice.component.css'
})
export class JuiceComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType2().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}
