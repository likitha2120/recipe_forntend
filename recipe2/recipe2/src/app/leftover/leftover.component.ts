import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-leftover',
  templateUrl: './leftover.component.html',
  styleUrl: './leftover.component.css'
})
export class LeftoverComponent implements  OnInit {
  products:any;
  constructor(private service:CustomerService){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType11().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}