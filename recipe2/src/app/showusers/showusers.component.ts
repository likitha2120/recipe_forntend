import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-showusers',
  templateUrl: './showusers.component.html',
  styleUrl: './showusers.component.css'
})
export class ShowusersComponent implements OnInit{
  EmployeeDetails: any;
  Items : any;
  deleteById:any;
  constructor(private service:CustomerService){
  }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    this.service.getCustomers().subscribe((data: any) => {
      this.EmployeeDetails = data;
      this.service.getAllItems().subscribe((data: any) => {
        this.Items = data;
      });
    });
  }

  deleteCustomerById(id: number) {
    this.service.deleteById(id).subscribe((data: any) => {
      this.deleteById = data; 
      this.getCustomers();
    });
  }
}