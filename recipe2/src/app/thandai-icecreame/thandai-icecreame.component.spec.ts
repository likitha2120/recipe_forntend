import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThandaiIcecreameComponent } from './thandai-icecreame.component';

describe('ThandaiIcecreameComponent', () => {
  let component: ThandaiIcecreameComponent;
  let fixture: ComponentFixture<ThandaiIcecreameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ThandaiIcecreameComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ThandaiIcecreameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
