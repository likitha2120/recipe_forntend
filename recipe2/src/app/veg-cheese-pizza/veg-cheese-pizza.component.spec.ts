import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VegCheesePizzaComponent } from './veg-cheese-pizza.component';

describe('VegCheesePizzaComponent', () => {
  let component: VegCheesePizzaComponent;
  let fixture: ComponentFixture<VegCheesePizzaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VegCheesePizzaComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VegCheesePizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
