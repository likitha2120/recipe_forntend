import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrinjalCurryComponent } from './brinjal-curry.component';

describe('BrinjalCurryComponent', () => {
  let component: BrinjalCurryComponent;
  let fixture: ComponentFixture<BrinjalCurryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BrinjalCurryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BrinjalCurryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
