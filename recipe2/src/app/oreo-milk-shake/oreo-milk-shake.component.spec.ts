import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OreoMilkShakeComponent } from './oreo-milk-shake.component';

describe('OreoMilkShakeComponent', () => {
  let component: OreoMilkShakeComponent;
  let fixture: ComponentFixture<OreoMilkShakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OreoMilkShakeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OreoMilkShakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
