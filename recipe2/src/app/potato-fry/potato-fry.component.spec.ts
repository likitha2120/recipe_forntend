import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PotatoFryComponent } from './potato-fry.component';

describe('PotatoFryComponent', () => {
  let component: PotatoFryComponent;
  let fixture: ComponentFixture<PotatoFryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PotatoFryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PotatoFryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
