import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FryComponent } from './fry.component';

describe('FryComponent', () => {
  let component: FryComponent;
  let fixture: ComponentFixture<FryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
