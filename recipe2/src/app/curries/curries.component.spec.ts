import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriesComponent } from './curries.component';

describe('CurriesComponent', () => {
  let component: CurriesComponent;
  let fixture: ComponentFixture<CurriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CurriesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CurriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
