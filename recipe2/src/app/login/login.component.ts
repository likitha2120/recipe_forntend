import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})

export class LoginComponent implements OnInit {
  customers: any;
  email: any;
  password: any;

  constructor(private router:Router,private service:CustomerService) {
   }
  ngOnInit(): void {
    this.service.getCustomers().subscribe((data:any)=>{
      this.customers=data
    })
  }

  loginValidate(form: any) {
    let isCredentialsValid = false;
    if (form.email === "likitha@gmail.com" && form.password === "1234") {
      isCredentialsValid = true;
      this.router.navigate(['/adminhome']);
    }
    else {
      for (let e of this.customers) {
        if (form.email == e.email && form.password == e.password) {
          isCredentialsValid = true;
          this.router.navigate(['/home']);
        }
      }
    }
    if (isCredentialsValid) {
      alert("login successful " + form.email);
    } else {
     alert("Invalid credentials!");
    }
  }
}