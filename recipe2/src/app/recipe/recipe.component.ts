import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrl: './recipe.component.css'
})
export class RecipeComponent implements OnInit {

  items:any;
  recipeForm: FormGroup | undefined;
  formBuilder: any;

  constructor(private service:CustomerService){
    
  }

  ngOnInit(): void {
    this.service.getAllItems().subscribe((data:any)=>{
      this.items=data;
      console.log(data);
    })
   
      this.recipeForm = this.formBuilder.group({
        categoryType: ['', Validators.required],
        recipeName: ['', Validators.required],
        recipeDescription: ['', Validators.required],
        cookingTime: ['', Validators.required],
        imgUrl: ['', Validators.required]
      });
    }
    
  }

