import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdlyComponent } from './idly.component';

describe('IdlyComponent', () => {
  let component: IdlyComponent;
  let fixture: ComponentFixture<IdlyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IdlyComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(IdlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
