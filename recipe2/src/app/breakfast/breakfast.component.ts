import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breakfast',
  templateUrl: './breakfast.component.html',
  styleUrl: './breakfast.component.css'
})
export class BreakfastComponent implements OnInit {
  products:any;
  constructor(private service:CustomerService, private router:Router){
  }
  ngOnInit(): void {
    this.service.getRecipeBycategoryType12().subscribe((data:any)=>{
      this.products=data;
      console.log(data); 
    })
  }
}

